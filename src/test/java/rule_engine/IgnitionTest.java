package rule_engine;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

public class IgnitionTest {
    Ignition ignition = new Ignition();
    HashMap<String, Object> map = new HashMap<String, Object>();

    @Before
    public void setUp() {
        map.put("age", 26);
        map.put("gender", "Male");
        map.put("past_order_amount", 200);
    }

    @Test
    public void isFalseForInput() {
        String s = "(age > 25 AND gender == \"fMale\") OR (past_order_amount > 10000)";
        Assert.assertFalse(ignition.isAllowed(s, "", map));
    }

    @Test
    public void isTrueForInput() {
        String s = "(age > 25 AND gender != \"fMale\") OR (past_order_amount > 10000)";
        Assert.assertTrue(ignition.isAllowed(s, "", map));
    }

    @Test
    public void isBetweenFalseForInput() {
        String s = "(past_order_amount between 400 and 500)";
        Assert.assertFalse(ignition.isAllowed(s, "", map));
    }

    @Test
    public void isBetweenTrueForInput() {
        String s = "(past_order_amount between 100 and 500)";
        Assert.assertTrue(ignition.isAllowed(s, "", map));
    }

    @Test
    public void isAllofFalseForInput() {
        String s = "([1,2,3] allof [1,2, 4, 5,6,7])";
        Assert.assertFalse(ignition.isAllowed(s, "", map));
    }

    @Test
    public void isAllofTrueForInput() {
        String s = "([1,2,3] allof [1,2,3,4,5,6,7])";
        Assert.assertTrue(ignition.isAllowed(s, "", map));
    }

    @Test
    public void isNoneofFalseForInput() {
        String s = "([1, 2, 3] noneof [1, 4, 5, 6, 7])";
        Assert.assertFalse(ignition.isAllowed(s, "", map));
    }

    @Test
    public void isNoneofTrueForInput() {
        String s = "([1, 2, 3] noneof [4, 5, 6, 7])";
        Assert.assertTrue(ignition.isAllowed(s, "", map));
    }
}