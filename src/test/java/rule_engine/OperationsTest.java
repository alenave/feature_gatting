package rule_engine;

import models.Hetero;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OperationsTest {
    Hetero a = new Hetero();
    Hetero b = new Hetero();
    Hetero r = new Hetero();

    @Before
    public void setUp() {
        a.setNumber(100);
        b.setNumber(200);
    }

    @Test
    public void isLessThanTrue() {
        r = Operations.spark("<", b, a);
        Assert.assertTrue(r.isBool());
    }

    @Test
    public void isLessThanFalse() {
        r = Operations.spark("<", a, b);
        Assert.assertFalse(r.isBool());
    }

    @Test
    public void isGreaterThanFalse() {
        r = Operations.spark(">", b, a);
        Assert.assertFalse(r.isBool());
    }

    @Test
    public void isGreaterThanTrue() {
        r = Operations.spark(">", a, b);
        Assert.assertTrue(r.isBool());
    }
}