package models;

import java.util.Map;

import static utils.Constants.*;

public class Hetero {
    int number;
    boolean bool;
    String string;

    public boolean isBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public static Hetero getHetero(String fieldName, Map<String, Object> user) {
        Hetero h = new Hetero();
        switch (fieldName.toLowerCase()) {

            case AGE:
            case PAST_ORDER_AMOUT:
                h.setNumber(Integer.parseInt(user.get(fieldName).toString()));
                break;
            case GENDER:
                h.setString(user.get(fieldName).toString());
                break;
            default:
                char[] tokens = fieldName.toLowerCase().toCharArray();
                boolean isDigit = true;
                for (char c : tokens) {
                    if (!Character.isDigit(c)) {
                        isDigit = false;
                        break;
                    }
                }
                if (isDigit)
                    h.setNumber(Integer.parseInt(fieldName));
                else
                    h.setString(fieldName);
        }
        return h;
    }
}