import java.util.HashMap;

import rule_engine.Ignition;
import utils.FeatureRuleMap;
import utils.GenerateConditionalExpression;

import static utils.Constants.SAME_DAY_DELIVERY;

public class KickStart {

    public static void main(String[] args) {
        HashMap<String, Object> userMap = new HashMap<String, Object>();
        userMap.put("age", 25);
        userMap.put("gender", "Male");
        userMap.put("past_order_amount", 3500);
//		String s = "(age == 20 AND gender != \"fMale\") and (past_order_amount between 100 and 10000)";
//		String s = "(past_order_amount between 100 and 500)";
//		String s = "([1,2,3] allof [1,2,3,4,5,6,7])";
//        String s = "([1, 2, 3] noneof [4, 5, 6, 7])";
        Ignition ignition = new Ignition();
        String conditionalExpression = new GenerateConditionalExpression().get(userMap, SAME_DAY_DELIVERY);
        System.out.println("User details -> " + userMap);
        System.out.println("Conditional Expression ->" + conditionalExpression);
        System.out.println(ignition.isAllowed(conditionalExpression, SAME_DAY_DELIVERY, userMap));
    }

}
