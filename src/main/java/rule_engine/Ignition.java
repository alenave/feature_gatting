package rule_engine;

import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;

import models.Hetero;
import utils.Refinery;

import static utils.Constants.*;


public class Ignition {
    private Stack<Hetero> values = new Stack<Hetero>();
    private Stack<String> ops = new Stack<String>();

    public boolean isAllowed(String conditionalExpression, String featureName, Map<String, Object> user) {
        if (featureName == null || conditionalExpression == null || user == null)
            return false;

        String refindFuel = new Refinery().getRefindOil(conditionalExpression);
        OperatorOperandSet operandSet = OperatorOperandSet.getInstance();
        StringTokenizer fuelTank = new StringTokenizer(refindFuel.toString().toLowerCase());
        while (fuelTank.hasMoreTokens()) {
            String drop = fuelTank.nextToken();

            if (operandSet.getFieldSet().contains(drop) || (isValue(drop) && !operandSet.getOperatorSet().contains(drop))) {
                values.push(Hetero.getHetero(drop, user));
            } else if (operandSet.getOperatorSet().contains(drop)) {
                if (operandSet.getSpecialOperatorSet().contains(drop)) {
                    handleSpecialEngine(fuelTank, drop);
                    continue;
                }
                while (!ops.empty() && Operations.isProperAssembled(drop, ops.peek()))
                    values.push(Operations.spark(ops.pop(), values.pop(), values.pop()));
                ops.push(drop);
            }
        }
        while (!ops.empty())
            values.push(Operations.spark(ops.pop(), values.pop(), values.pop()));
        return values.pop().isBool();
    }

    private boolean isValue(String str) {
        char[] tokens = str.toLowerCase().toCharArray();
        if ((tokens[0] >= '0' && tokens[0] <= '9') || (tokens[0] >= 'a' && tokens[0] <= 'z'))
            return true;
        return false;
    }

    private void handleSpecialEngine(StringTokenizer fuelTank, String drop) {
        String first = fuelTank.nextToken();
        if (drop.equalsIgnoreCase(BETWEEN)) {
            fuelTank.nextToken();
            String second = fuelTank.nextToken();
            values.push(Operations.sparkBetween(values.pop(), first, second));
        } else if (drop.equalsIgnoreCase(ALLOF) || drop.equalsIgnoreCase(NONEOF)) {
            values.push(Operations.sparkEnumSet(drop, values.pop().getString(), first));
        }
    }
}
