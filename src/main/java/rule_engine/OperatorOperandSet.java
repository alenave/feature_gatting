package rule_engine;

import java.util.HashSet;

import static utils.Constants.*;

public class OperatorOperandSet {
    private static OperatorOperandSet single_instance = null;
    private HashSet<String> fieldSet = new HashSet<String>();
    private HashSet<String> operatorSet = new HashSet<String>();
    private HashSet<String> specialOperator = new HashSet<String>();

    private OperatorOperandSet() {
        fieldSet.add(AGE);
        fieldSet.add(MALE);
        fieldSet.add(PAST_ORDER_AMOUT);
        operatorSet.add(AND);
        operatorSet.add(OR);
        operatorSet.add(GREATER_THAN);
        operatorSet.add(GREATER_THAN_EQUAL_TO);
        operatorSet.add(LESS_THAN);
        operatorSet.add(LESS_THAN_EQUAL_TO);
        operatorSet.add(EQUAL_TO);
        operatorSet.add(NOT_EQUAL_TO);
        operatorSet.add(BETWEEN);
        operatorSet.add(ALLOF);
        operatorSet.add(NONEOF);
        specialOperator.add(BETWEEN);
        specialOperator.add(ALLOF);
        specialOperator.add(NONEOF);
    }

    public static OperatorOperandSet getInstance() {
        if (single_instance == null)
            single_instance = new OperatorOperandSet();

        return single_instance;
    }

    public HashSet<String> getFieldSet() {
        return fieldSet;
    }

    public HashSet<String> getOperatorSet() {
        return operatorSet;
    }

    public HashSet<String> getSpecialOperatorSet() {
        return specialOperator;
    }
}
