package rule_engine;

import models.Hetero;

import java.util.Collections;
import java.util.HashSet;
import java.util.StringTokenizer;

import static utils.Constants.*;

public class Operations {
    public static Hetero spark(String operator, Hetero b, Hetero a) {
        Hetero h = new Hetero();
        switch (operator) {
            case LESS_THAN:
                h.setBool(a.getNumber() < b.getNumber());
                break;
            case GREATER_THAN:
                h.setBool(a.getNumber() > b.getNumber());
                break;
            case GREATER_THAN_EQUAL_TO:
                h.setBool(a.getNumber() >= b.getNumber());
                break;
            case LESS_THAN_EQUAL_TO:
                h.setBool(a.getNumber() <= b.getNumber());
                break;
            case EQUAL_TO:
                if (a.getString() != null)
                    h.setBool(a.getString().equalsIgnoreCase(b.getString()));
                else
                    h.setBool(a.getNumber() == b.getNumber());
                break;
            case NOT_EQUAL_TO:
                if (a.getString() != null)
                    h.setBool(!a.getString().equalsIgnoreCase(b.getString()));
                else
                    h.setBool(a.getNumber() != b.getNumber());
                break;
            case AND:
                h.setBool(a.isBool() && b.isBool());
                break;
            case OR:
                h.setBool(a.isBool() || b.isBool());
                break;
        }
        return h;
    }

    public static Hetero sparkBetween(Hetero field, String first, String second) {
        Hetero h = new Hetero();
        int fieldValue = field.getNumber();
        h.setBool(fieldValue >= Integer.parseInt(first) && fieldValue <= Integer.parseInt(second));
        return h;
    }

    public static Hetero sparkEnumSet(String operator, String desired, String all) {
        Hetero h = new Hetero();
        h.setBool(evaluateEnumSet(operator, desired, all));
        return h;
    }


    public static boolean isProperAssembled(String op1, String op2) {
        if (op2.equals("(") || op2.equals(")"))
            return false;
        if (higherPrecedenceSet.contains(op1) && lowerPrecedenceSet.contains(op2))
            return false;
        else
            return true;
    }

    private static boolean evaluateEnumSet(String operator, String desiredValues, String allValues) {
        desiredValues = desiredValues.replace('[', ' ');
        desiredValues = desiredValues.replace(']', ' ');
        allValues = allValues.replace('[', ' ');
        allValues = allValues.replace(']', ' ');
        HashSet<String> desiredSet = new HashSet<>();
        HashSet<String> allSet = new HashSet<>();
        StringTokenizer desiredTokens = new StringTokenizer(desiredValues.toLowerCase(), ",");
        while (desiredTokens.hasMoreTokens()) {
            desiredSet.add(desiredTokens.nextToken());
        }

        StringTokenizer allTokens = new StringTokenizer(allValues.toLowerCase(), ",");
        while (allTokens.hasMoreTokens()) {
            allSet.add(allTokens.nextToken().trim());
        }

        if (operator.equalsIgnoreCase(ALLOF) && allSet.containsAll(desiredSet)) {
            return true;
        } else if (operator.equalsIgnoreCase(NONEOF) && Collections.disjoint(desiredSet, allSet)) {
            return true;
        }

        return false;
    }
}