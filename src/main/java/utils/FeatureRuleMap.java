package utils;

import org.omg.CORBA.PUBLIC_MEMBER;

import java.util.HashMap;

import static utils.Constants.*;

public class FeatureRuleMap {
    private static FeatureRuleMap single_instance = null;
    HashMap<String, Object> ruleMap = new HashMap<String, Object>();
    HashMap<String, HashMap<String, Object>> map = new HashMap<String, HashMap<String, Object>>();

    private FeatureRuleMap() {
        ruleMap.put(AGE, 25);
        ruleMap.put(AGE + AGE, BETWEEN + 25 + AND + 35);
        ruleMap.put(GENDER, MALE);
        ruleMap.put(GENDER + GENDER, EQUAL_TO);
        ruleMap.put(PAST_ORDER_AMOUT, 10000);
        ruleMap.put(PAST_ORDER_AMOUT + PAST_ORDER_AMOUT, GREATER_THAN_EQUAL_TO);
        ruleMap.put(CITY, "Bangalore");
        ruleMap.put(CITY + CITY, NOT_EQUAL_TO);
        map.put(SAME_DAY_DELIVERY, ruleMap);

        ruleMap.put(AGE, 22);
        ruleMap.put(AGE + AGE, GREATER_THAN);
        ruleMap.put(GENDER, MALE);
        ruleMap.put(GENDER + GENDER, EQUAL_TO);
        ruleMap.put(PAST_ORDER_AMOUT, 3000);
        ruleMap.put(PAST_ORDER_AMOUT + PAST_ORDER_AMOUT, GREATER_THAN_EQUAL_TO);
        ruleMap.put(CITY, "Bangalore");
        ruleMap.put(CITY + CITY, EQUAL_TO);
        map.put(CREDIT_CARD_OFFERS, ruleMap);
    }

    public static FeatureRuleMap getInstance() {
        if (single_instance == null)
            single_instance = new FeatureRuleMap();

        return single_instance;
    }

    public HashMap<String, HashMap<String, Object>> getFeatureRuleMap() {
        return map;
    }
}
