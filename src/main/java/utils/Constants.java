package utils;

import java.util.Arrays;
import java.util.HashSet;

public class Constants {
    public static final String BETWEEN = "between";
    public static final String ALLOF = "allof";
    public static final String NONEOF = "noneof";
    public static final String AGE = "age";
    public static final String MALE = "male";
    public static final String FEMALE = "female";
    public static final String GENDER = "gender";
    public static final String PAST_ORDER_AMOUT = "past_order_amount";
    public static final String AND = "and";
    public static final String OR = "or";
    public static final String GREATER_THAN = ">";
    public static final String LESS_THAN = "<";
    public static final String GREATER_THAN_EQUAL_TO = ">=";
    public static final String LESS_THAN_EQUAL_TO = "<=";
    public static final String EQUAL_TO = "==";
    public static final String NOT_EQUAL_TO = "!=";
    private static String[] higherPrecedenceOperators = {GREATER_THAN, GREATER_THAN_EQUAL_TO, LESS_THAN,
            LESS_THAN_EQUAL_TO, NOT_EQUAL_TO, EQUAL_TO};
    public static final HashSet<String> higherPrecedenceSet = new HashSet<String>(Arrays.asList(higherPrecedenceOperators));
    private static String[] lowerPrecedenceOperators = {AND, OR};
    public static final HashSet<String> lowerPrecedenceSet = new HashSet<String>(Arrays.asList(lowerPrecedenceOperators));
    public static final String CITY = "city";
    public static final String SAME_DAY_DELIVERY = "same_day_delivery";
    public static final String CREDIT_CARD_OFFERS = "credit_card_offers";
    public static final String SPACE = " ";
}
