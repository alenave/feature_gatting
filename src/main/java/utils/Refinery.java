package utils;

public class Refinery {
    public String getRefindOil(String crudeOil) {
        return crudeOil.replace('(', ' ')
                .replace(')', ' ')
                .replace('[', ' ')
                .replace(']', ' ')
                .replace(", ", ",")
                .replace('"', ' ');
    }
}
