package utils;

import org.jcp.xml.dsig.internal.dom.ApacheNodeSetData;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static utils.Constants.AND;
import static utils.Constants.SPACE;

public class GenerateConditionalExpression {
    public String get(HashMap<String, Object> user, String feature) {
        HashMap<String, Object> featureRuleMap = FeatureRuleMap.getInstance().getFeatureRuleMap().get(feature);
        StringBuilder rule = new StringBuilder();
        Iterator<Map.Entry<String, Object>> itr = user.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, Object> entry = itr.next();
            String key = entry.getKey();
            rule.append(entry.getValue() + SPACE);
            String conditionKey = key + key;
            rule.append(featureRuleMap.get(conditionKey) + SPACE);
            rule.append(featureRuleMap.get(key) + SPACE);
            if (itr.hasNext())
                rule.append(AND + SPACE);
        }
        return rule.toString();
    }
}
